package org.scrum.psd.battleship.controller;

import org.junit.Assert;
import org.junit.Test;
import org.scrum.psd.battleship.controller.dto.Color;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.Arrays;

public class ShipTest {

    @Test
    public void testIsSunk() {
        Letter letter = Letter.values()[0];
        Ship ship =  new Ship("Aircraft Carrier", 5, Color.CADET_BLUE);
        ship.getPositions().add(new Position(letter, 1));

        Ship result = GameController.shoot( Arrays.asList(ship), new Position(Letter.A, 1));
        Assert.assertTrue(result.isSunk());

    }
}
